# Todo Web App
Simple web app to mainly learn docker & docker-compose.
Also I get knowledge about connect backend app in *Django* and frontend app in *Angular* 

App is mainly based on this [tutorial](https://dragonprogrammer.com/dockerized-django-api-angular-tutorial/)

But I added some extra things in frontend imporving design and UX *(Clarity)* So now, this web app looks like this:

![screebshot](app.png)