/* tslint:disable:variable-name */
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Task } from '../task';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  // $ - naming convention for observables, put at the end
  tasks$: Observable<Task[]>;
  task_form: FormGroup;

  creationMode = false;
  removeMode = false;
  editMode = false;
  current_task: Task;
  edited_task: Task;

  constructor(private apiService: ApiService, private form_builder: FormBuilder) { }

  ngOnInit() {
    this.getTasks();
    this.current_task = this.tasks$[0];


    this.task_form = this.form_builder.group({
      title: '',
      content: ''
    });

    // Set validators for fields.
    this.task_form.controls.title.setValidators([Validators.required]);
    this.task_form.controls.content.setValidators([Validators.required]);
  }

  onSubmit() {
    this.creationMode = false;
    // Create the Task.
    this.apiService.postTask(this.task_form.value)
      .subscribe(
        (response) => {
          console.log(response);
          this.getTasks();
        }
      );
  }

  updateTask(task) {
    this.editMode = false;
    this.apiService.putTask(task)
      .subscribe(
        (response) => {
          console.log(response);
          this.getTasks();
        }
      );
  }

  deleteTask(task_id: number) {
    this.removeMode = false;
    this.apiService.deleteTask(task_id)
      .subscribe(
        (response) => {
          console.log(response);
          this.getTasks();
        }
      );
  }

  public getTasks() {
    this.tasks$ = this.apiService.getTasks();
  }

  switchToEditMode(task_parameter: Task) {
    this.editMode = true;
    const edited = {
      id: task_parameter.id,
      title: task_parameter.title,
      content: task_parameter.content,
      created_on: task_parameter.created_on,
      due_date: task_parameter.due_date
    } as Task;

    this.edited_task = edited;
  }

}
