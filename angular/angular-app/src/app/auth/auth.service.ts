import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as auth0 from 'auth0-js';

import { environment } from '../../environments/environment';

(window as any).global = window;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  requestedScopes: string = 'openid profile';

  // Create Auth0 web auth instance
  auth0 = new auth0.WebAuth({
    clientID: environment.auth.clientID,
    domain: environment.auth.domain,
    responseType: 'token id_token',
    redirectUri: environment.auth.auth0RedirectUri,
    audience: environment.auth.audience,
    scope: this.requestedScopes
  });

  // Store authentication data
  constructor(public router: Router) { }

  public login() {
    this.auth0.authorize();
  }

  // Looks for result of authentication in URL hash; result is processed in parseHash.
  public handleLoginCallback(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        // this.getUserInfo(authResult);
        this.setSession(authResult);
      } else if (err) {
        console.log(`Error: ${err.error}`);
      }
      this.router.navigate(['/']); // Redirect the user after the session is set up.
    });
  }

  private setSession(authResult): void {
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    const scopes = authResult.scope || this.requestedScopes || '';

    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('scopes', JSON.stringify(scopes));
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('scopes');
    // Go back to the home route
    this.router.navigate(['/']);
  }
  

  // getAccessToken() {
  //   this.auth0.checkSession({}, (err, authResult) => {
  //     if (authResult && authResult.accessToken) {
  //       this.getUserInfo(authResult);
  //     }
  //   });
  // }

  // // Use access token to retrieve user's profile and set session
  // getUserInfo(authResult) {
  //   this.auth0.client.userInfo(authResult.accessToken, (err, profile) => {
  //     if (profile) {
  //       this.setSession(authResult, profile);
  //     }
  //   });
  // }

  // // Save authentication data and update login status subject
  // private setSession(authResult, profile): void {
  //   this.expiresAt = authResult.expiresIn * 1000 + Date.now();
  //   this.accessToken = authResult.accessToken;
  //   this.userProfile = profile;
  //   this.authenticated = true;
  // }

  // // Log out of Auth0 session
  // // Ensure that returnTo URL is specified in Auth0
  // // Application settings for Allowed Logout URLs
  // public logout(): void {
  //   this.auth0.logout({
  //     returnTo: environment.auth.auth0ReturnTo,
  //     clientID: environment.auth.clientID
  //   });
  // }

  // Checks whether the expiry time for the user's Access Token has passed and that user is signed in locally.
  get isLoggedIn(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  public userHasScopes(scopes: Array<string>): boolean {
    const grantedScopes = JSON.parse(localStorage.getItem('scopes')).split(' ');
    return scopes.every(scope => grantedScopes.includes(scope));
  }

}