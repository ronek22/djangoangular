from django.db import models

from datetime import date

class Task(models.Model):
    """Stores a task."""
    created_by = models.CharField(max_length=200)

    title = models.CharField(max_length=50)
    content = models.CharField(max_length=50)

    # Date the task was created
    created_on = models.DateField(default=date.today)

    # Due date
    due_date = models.DateField(default=date.today)

    # Meta data about the database table
    class Meta:
        db_table = 'task'
        ordering = ['id']

    def __str__(self):
        return self.title
